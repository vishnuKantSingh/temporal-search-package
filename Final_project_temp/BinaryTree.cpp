#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <utility>

#include "Node.h"
#include "Global.h"
#include "BinaryTree.h"
#include "ValueNode.h"
#include "BinaryNode.h"
#include "SignalNode.h"
#include "TernaryNode.h"
#include "TernaryKeywordNode.h"

//  This gives the keyword of Token symbol.
TypeofKeyword getkeyWord(std::string str)
{
    std::map<TypeofKeyword, std::regex>::const_iterator itr;
    for (itr = KeywordSymbol.begin(); itr != KeywordSymbol.end(); itr++)
    {
        if (regex_match(str, itr->second))
        {
            return itr->first;
        }
    }
    return TypeofKeyword::SIGNAME;
}

//  This gives the class of Token symbol.
TypeofClass getclasstype(TypeofKeyword keyword)
{
    std::map<TypeofKeyword, TypeofClass>::const_iterator itr;
    for (itr = classfromkeyword.begin(); itr != classfromkeyword.end(); itr++)
    {
        if (keyword == itr->first)
        {
            return itr->second;
        }
    }
    return TypeofClass::UNKNOWN;
}

// This is the constructor for construct the expression tree.
BinaryTree::BinaryTree(std::vector<std::string> postfixNotationTokens)
{
    int size = postfixNotationTokens.size();
    std::stack<Node *> oprndStack;

    if (postfixNotationTokens.size() == 0)
    {
        std::cout << "\n"
                  << "vector is empty so tree can not be made ."
                  << "\n";
        head = NULL;
    }

    for (int i = 0; i < size; i++)
    {
        TypeofKeyword keyword = getkeyWord(postfixNotationTokens[i]);
        TypeofClass classtype = getclasstype(keyword);

        if (classtype == TypeofClass::VALUENODE)
        {

            ValueNode *v = new ValueNode(postfixNotationTokens[i], NULL, NULL);
            oprndStack.push(v);
            std::cout << v->value << " : node created and pushed to oprnd_stack (VALUE)."
                      << "\n"
                      << "\n";
        }
        else if ((classtype == TypeofClass::BINARYNODE) && (keyword == TypeofKeyword::NOT))
        {
            Node *temp1 = oprndStack.top();
            oprndStack.pop();
            std::cout << temp1->value << " : node pooped out from oprnd_stack and will go in right"
                      << "\n";

            BinaryNode *b = new BinaryNode(postfixNotationTokens[i], NULL, temp1);
            oprndStack.push(b);
            std::cout << b->value << " : node created and pushed to oprnd_stack (NOT)."
                      << "\n"
                      << "\n";
        }
        else if (classtype == TypeofClass::BINARYNODE)
        {
            Node *temp1 = oprndStack.top();
            oprndStack.pop();
            Node *temp2 = oprndStack.top();
            oprndStack.pop();
            std::cout << temp1->value << " : node pooped out from oprnd_stack and will go in right."
                      << "\n";
            std::cout << temp2->value << " : node popped out from oprnd_stack and will go in left."
                      << "\n";

            BinaryNode *b = new BinaryNode(postfixNotationTokens[i], temp2, temp1);
            oprndStack.push(b);
            std::cout << b->value << " : node created and pushed to oprnd_stack (MULTIPLE)."
                      << "\n"
                      << "\n";
        }
        else if (classtype == TypeofClass::TERNARYNODE)
        {
            Node *temp1 = oprndStack.top();
            oprndStack.pop();
            Node *temp2 = oprndStack.top();
            oprndStack.pop();
            std::cout << temp1->value << " : node pooped out from oprnd_stack and will go in right."
                      << "\n";
            std::cout << temp2->value << " : node popped out from oprnd_stack and will go in left."
                      << "\n";

            TernaryNode *t = new TernaryNode(postfixNotationTokens[i], temp2, temp1);
            oprndStack.push(t);
            std::cout << t->value << " : node created and pushed to oprnd_stack (FOLLOWEDBY)."
                      << "\n"
                      << "\n";
        }
        else if ((classtype == TypeofClass::TERNERYKEYWORDNODE) && (keyword == TypeofKeyword::SAMECYCLE))
        {
            Node *temp1 = oprndStack.top();
            oprndStack.pop();
            std::cout << temp1->value << " : node popped out from oprnd_stack and will go in left."
                      << "\n";

            TernaryKeywordNode *tk = new TernaryKeywordNode(postfixNotationTokens[i], temp1, NULL);
            oprndStack.push(tk);
            std::cout << tk->value << " : node created and pushed to oprnd_stack (SAMECYCLE)."
                      << "\n"
                      << "\n";
        }
        else if ((classtype == TypeofClass::TERNERYKEYWORDNODE) && ((keyword == TypeofKeyword::OVER) || (keyword == TypeofKeyword::UNDER)))
        {
            Node *temp1 = oprndStack.top();
            oprndStack.pop();
            Node *temp2 = oprndStack.top();
            oprndStack.pop();
            std::cout << temp1->value << " : node pooped out from oprnd_stack and will go in right."
                      << "\n";
            std::cout << temp2->value << " : node popped out from oprnd_stack and will go in left."
                      << "\n";

            TernaryKeywordNode *tk = new TernaryKeywordNode(postfixNotationTokens[i], temp2, temp1);
            oprndStack.push(tk);
            std::cout << tk->value << " : node created and pushed to oprnd_stack (OVER/UNDER)."
                      << "\n"
                      << "\n";
        }

        else if ((classtype == TypeofClass::TERNERYKEYWORDNODE) && (keyword == TypeofKeyword::BETWEEN))
        {
            Node *temp1 = oprndStack.top();
            oprndStack.pop();
            Node *temp2 = oprndStack.top();
            oprndStack.pop();
            std::cout << temp1->value << " : node popped out from oprnd_stack and will go in right."
                      << "\n";
            std::cout << temp2->value << " : node popped out from oprnd_stack and will go in root."
                      << "\n";

            TernaryKeywordNode *tk1 = new TernaryKeywordNode(temp2->value, NULL, temp1);
            oprndStack.push(tk1);
            std::cout << tk1->value << " : node created and pushed to oprnd_stack."
                      << "\n";

            Node *temp3 = oprndStack.top();
            oprndStack.pop();
            Node *temp4 = oprndStack.top();
            oprndStack.pop();

            std::cout << temp3->value << " : node popped out from oprnd_stack and will go in right."
                      << "\n";
            std::cout << temp4->value << " : node popped out from oprnd_stack and will go in left."
                      << "\n";

            TernaryKeywordNode *tk2 = new TernaryKeywordNode(postfixNotationTokens[i], temp4, temp3);
            oprndStack.push(tk2);
            std::cout << tk2->value << " : node created and pushed to oprnd_stack (BETWEEN)."
                      << "\n"
                      << "\n";
        }

        else
        {
            SignalNode *s = new SignalNode(postfixNotationTokens[i], NULL, NULL);
            oprndStack.push(s);
            std::cout << s->value << " : node created and pushed to oprnd_stack (SIGNAL)."
                      << "\n"
                      << "\n";
        }
    }
    head = oprndStack.top();
    oprndStack.pop();
}

// This will traverse the whole tree in post order and will return a pair of string in which first string will be sql query string for input expression.
std::pair<std::string, std::string> BinaryTree::PostOrderTraversal(Node *root, Node *parent)
{
    if (root == NULL)
    {
        return {"", ""};
    }
    else
    {
        std::pair<std::string, std::string> leftReturn = PostOrderTraversal(root->left, root);
        std::pair<std::string, std::string> rightReturn = PostOrderTraversal(root->right, root);

        TypeofKeyword keyword = getkeyWord(root->value);
        TypeofClass classtype = getclasstype(keyword);
        TypeofKeyword keywordofParent;
        if (parent != NULL)
        {
            keywordofParent = getkeyWord(parent->value);
        }

        if (classtype == TypeofClass::SIGNALNODE)
        {
            SignalNode object1;
            return object1.getSqlQueryString(leftReturn, rightReturn, root->value, keyword, keywordofParent);
        }
        else if (classtype == TypeofClass::VALUENODE)
        {
            ValueNode object1;
            return object1.getSqlQueryString(leftReturn, rightReturn, root->value, keyword, keywordofParent);
        }
        else if (classtype == TypeofClass::BINARYNODE)
        {
            BinaryNode object1;
            return object1.getSqlQueryString(leftReturn, rightReturn, root->value, keyword, keywordofParent);
        }
        else if (classtype == TypeofClass::TERNARYNODE)
        {
            TernaryNode object1;
            return object1.getSqlQueryString(leftReturn, rightReturn, root->value, keyword, keywordofParent);
        }
        else if (classtype == TypeofClass::TERNERYKEYWORDNODE)
        {
            TernaryKeywordNode object1;
            return object1.getSqlQueryString(leftReturn, rightReturn, root->value, keyword, keywordofParent);
        }
        else
        {
            std::pair<std::string, std::string> temp_pair = {"", ""};
            return temp_pair;
        }
    }
}

// this will deallocate the memory of constructed expression tree.
void BinaryTree::deallocateMemoryoftree(Node *root){
    if(root == NULL)return;
    deallocateMemoryoftree(root->left);
    deallocateMemoryoftree(root->right);
    free(root);
}

// destructor for the created object.
BinaryTree::~BinaryTree(){
    deallocateMemoryoftree(head);
}

