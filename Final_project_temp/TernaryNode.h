#include <string>
#include <vector>
#include <utility>

#include "Global.h"
#include "Node.h"

#ifndef TERNARYNODE_H
#define TERNARYNODE_H


// TernaryNode contain token only {followedby}.
class TernaryNode : public Node{
    public:
    // default constructor
    TernaryNode():Node(){};
    
    //parameterized constructor
    TernaryNode(std::string value,Node* l, Node* r) : Node(value , l, r){};

    public:
        
        // This function will return pair of string in which first string will contain sql query from left node and second string will have sql query from right node.
        std::pair<std::string , std::string> getSqlQueryString(std::pair<std::string , std::string> returnedLeftpair , std::pair<std::string , std::string> returnedRightpair , std::string temp, TypeofKeyword keyword , TypeofKeyword keywordofParent);
    
};

#endif
