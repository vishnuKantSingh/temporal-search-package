#include <string>
#include <vector>

#ifndef BASECLASS_H
#define BASECLASS_H

class Node
{
public:
    std::string value; // value of a node in the form of string.
    Node *left;        // pointer to the left child.
    Node *right;       // pointer to the right child.
    
    //default constructor
    Node(){
        // nothing here
    };
    // Parameterised constructor for a node.
    Node(std::string nodevalue, Node *l, Node *r)
    {
        value = nodevalue;
        left = l;
        right = r;
    }
    
};

#endif
