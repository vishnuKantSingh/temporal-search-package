#include <regex>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <set>

#ifndef GLOBAL_H
#define GLOBAL_H


enum class TypeofKeyword {NOTEQUAL, NOT, EQUAL, AND, OR, FOLLOWEDBY, SAMECYCLE, UNDER, OVER, BETWEEN, SIGNAME ,VALUE};
enum class TypeofClass {SIGNALNODE, VALUENODE ,BINARYNODE, TERNARYNODE, TERNERYKEYWORDNODE, UNKNOWN};

// mapping from operator to operator with space.
static const std::map<std::string , std::string> OperatorWithSpace = {{"=="," == "},{"!="," != "},{"!(\\w)"," ! $1"},{"\\("," ( "},{"\\)"," ) "}};

// mapping from keyword to it's symbol present in input expression.
static const std::map<TypeofKeyword , std::regex> KeywordSymbol = {{TypeofKeyword::NOTEQUAL, std::regex("!=")}, {TypeofKeyword::NOT, std::regex("!")}, {TypeofKeyword::EQUAL, std::regex("==")}, {TypeofKeyword::AND, std::regex("and", std::regex_constants::icase)}, {TypeofKeyword::OR, std::regex("or", std::regex_constants::icase)}, {TypeofKeyword::FOLLOWEDBY, std::regex("FollowedBy", std::regex_constants::icase)}, {TypeofKeyword::SAMECYCLE, std::regex("SameCycle", std::regex_constants::icase)}, {TypeofKeyword::UNDER, std::regex("Under", std::regex_constants::icase)}, {TypeofKeyword::OVER, std::regex("Over", std::regex_constants::icase)}, {TypeofKeyword::BETWEEN, std::regex("Between", std::regex_constants::icase)}, {TypeofKeyword::VALUE , std::regex("-?(\\d)?'[b|d|h|o]?[\\d|x|_|z|a|b|c|d|e|f]+|[0-9]+")}};
 

// // mapping from class to it's symbol present in input expression.
static const std::map<TypeofKeyword , TypeofClass> classfromkeyword = {{TypeofKeyword::SIGNAME , TypeofClass::SIGNALNODE},{TypeofKeyword::EQUAL, TypeofClass::BINARYNODE}, {TypeofKeyword::NOT , TypeofClass::BINARYNODE}, {TypeofKeyword::NOTEQUAL, TypeofClass::BINARYNODE}, {TypeofKeyword::AND , TypeofClass::BINARYNODE}, {TypeofKeyword::OR , TypeofClass::BINARYNODE}, {TypeofKeyword::FOLLOWEDBY , TypeofClass::TERNARYNODE}, {TypeofKeyword::SAMECYCLE , TypeofClass::TERNERYKEYWORDNODE}, {TypeofKeyword::UNDER , TypeofClass::TERNERYKEYWORDNODE}, {TypeofKeyword::OVER , TypeofClass::TERNERYKEYWORDNODE}, {TypeofKeyword::BETWEEN , TypeofClass::TERNERYKEYWORDNODE}, {TypeofKeyword::VALUE , TypeofClass::VALUENODE}};

// mapping from token operators to its regex.
static const std::map<std::string , std::regex> TokenSymbol = {{"NOTEQUAL", std::regex("!=")}, {"NOT", std::regex("!")}, {"EQUAL", std::regex("==")}, {"AND", std::regex("and", std::regex_constants::icase)}, {"OR", std::regex("or", std::regex_constants::icase)}, {"FOLLOWEDBY", std::regex("FollowedBy", std::regex_constants::icase)}, {"SAMECYCLE", std::regex("SameCycle", std::regex_constants::icase)}, {"UNDER", std::regex("Under", std::regex_constants::icase)}, {"OVER", std::regex("Over", std::regex_constants::icase)}, {"BETWEEN", std::regex("Between", std::regex_constants::icase)}};


// This map will have all operators as second arguments.
static const std::map<int , std::regex> opSet = {{1, std::regex("!=")}, {2, std::regex("!")}, {3, std::regex("==")}, {4, std::regex("and", std::regex_constants::icase)}, {5, std::regex("or", std::regex_constants::icase)}, {6, std::regex("FollowedBy", std::regex_constants::icase)}, {7, std::regex("SameCycle", std::regex_constants::icase)}, {8, std::regex("Under", std::regex_constants::icase)}, {9, std::regex("Over", std::regex_constants::icase)}, {10, std::regex("Between", std::regex_constants::icase)} };



// mapping from keyword to its sql equivalant.
static const std::map<TypeofKeyword , std::string> Keywordtosql = {{TypeofKeyword::NOT, "NOT"}, {TypeofKeyword::NOTEQUAL, "NOT IN"}, {TypeofKeyword::EQUAL, "AND"}, {TypeofKeyword::AND, "AND"}, {TypeofKeyword::OR, "OR"}, {TypeofKeyword::FOLLOWEDBY, "AND" }, {TypeofKeyword::SAMECYCLE, "SAMECYCLE" }, {TypeofKeyword::UNDER, "<"}, {TypeofKeyword::OVER, ">"}, {TypeofKeyword::BETWEEN, "BETWEEN" }};

// mapping from keyword operators to its precedence.
static const std::map<TypeofKeyword, int> precedence = {{TypeofKeyword::NOT, 5}, {TypeofKeyword::NOTEQUAL, 4}, {TypeofKeyword::EQUAL, 4}, {TypeofKeyword::AND, 3}, {TypeofKeyword::OR, 2}, {TypeofKeyword::FOLLOWEDBY, 1}, {TypeofKeyword::SAMECYCLE, 1}, {TypeofKeyword::UNDER, 1}, {TypeofKeyword::OVER, 1}, {TypeofKeyword::BETWEEN,1}};



#endif