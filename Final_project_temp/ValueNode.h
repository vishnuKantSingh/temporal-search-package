#include <string>
#include <vector>
#include <utility>

#include "Global.h"
#include "Node.h"

#ifndef VALUENODE_H
#define VALUENODE_H


// ValueNode contain token of values in the form of string as (2'10 : this is binary value , (3'd456 or 456) this is decimal value etc.) 
class ValueNode : public Node{
    public:
    //ValueNode();
        ValueNode():Node(){};
        ValueNode(std::string value,Node* l, Node* r) : Node(value , l, r){};
    public:
       
        // This function will return pair of string in which first string will contain int value in form of string and other string would be empty.
        std::pair<std::string , std::string> getSqlQueryString(std::pair<std::string , std::string> returnedLeftpair , std::pair<std::string , std::string> returnedRightpair , std::string temp, TypeofKeyword keyword , TypeofKeyword keywordofParent);
    
};


#endif
