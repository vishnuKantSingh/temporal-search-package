#include <iostream>
#include <string>
#include <vector>
#include<regex>
#include "Global.h"

#ifndef TOKENIZER_H
#define TOKENIZER_H

class Tokenizer {
    public:
        std::string inputExp;
        std::vector<std::string> allTokens;

        // CONSTRUCTOR FOR TOKENIZE THE STRING.
        Tokenizer(std::string str);  

        // this will return keyword type of token exp: (== -> EQUAL etc).
	static TypeofKeyword getKeywordType(std::string token);
};

#endif