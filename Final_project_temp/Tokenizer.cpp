#include <iostream>
#include <string>
#include <sstream>
#include <regex>
#include <map>

#include "Tokenizer.h"
#include "Global.h"

Tokenizer::Tokenizer(std::string in_string){
    inputExp = in_string;

    std::cout<<"input string is : "<<inputExp<<"\n";
    std::cout<<"\n";
    std::string new_string;
     
    // This is use to put spaces between operand and operator and open and close braces.
    std::map<std::string , std::string>::const_iterator itr;
     for(itr = OperatorWithSpace.begin(); itr != OperatorWithSpace.end(); itr++){
         new_string = std::regex_replace(inputExp, std::regex(itr->first), itr->second);
         inputExp = new_string;
     }
    std::cout<<"input string with space is : "<<inputExp<<"\n";
    std::cout<<"\n";

    // input expression will be converted into tokens and will store in a vector.
    std::stringstream words(inputExp);
    std::string temp;
    
    while(words >> temp){
        allTokens.push_back(temp);
    }
    for(int i=0;i<allTokens.size();i++){
        std::cout<<"Infix Token "<<i+1<<" : "<<allTokens[i]<<"\n";
    }
    std::cout<<"\n";

}

// it will return keyword of token which is defined in global.h
TypeofKeyword Tokenizer::getKeywordType(std::string token){
    std::map<TypeofKeyword , std::regex>::const_iterator itr;
    for(itr = KeywordSymbol.begin(); itr != KeywordSymbol.end() ; itr++){
        if(std::regex_match(token , std::regex(itr->second) )){return itr->first;}
    }
    return TypeofKeyword::SIGNAME ;

}




