#! /bin/bash

echo "Compiling C++ files"

g++ -o tmpSearch main.cpp Tokenizer.cpp BinaryTree.cpp SignalNode.cpp ValueNode.cpp BinaryNode.cpp TernaryNode.cpp TernaryKeywordNode.cpp -lsqlite3

echo "Compilation done"

./tmpSearch > output.txt