#include <string>
#include <map>
#include<vector>
#include<utility>


#include "TernaryNode.h"
#include "Global.h"
#include "Node.h"

// Returns the SQL query (for a node which contains a ternary operator which is FOLLOWEDBY) as a first string of returning pair.
std::pair<std::string , std::string> TernaryNode::getSqlQueryString(std::pair<std::string , std::string> returnedLeftPair , std::pair<std::string , std::string> returnedRightPair , std::string temp, TypeofKeyword keywordType ,TypeofKeyword keywordtypeofParent){
    std::pair<std::string , std::string> returnpair;
    std::string firststring = returnedLeftPair.first; 
    std::string secondstring = returnedRightPair.first;
    std::cout<<" keyword of current node is followedby \n";
    returnpair.first = firststring;
    returnpair.second = secondstring;
     
    return returnpair; 
}
