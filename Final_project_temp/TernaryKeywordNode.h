#include <string>
#include <vector>
#include <utility>

#include "Global.h"
#include "Node.h"

#ifndef TERNARYKEYWORDNODE_H
#define TERNARYKEYWORDNODE_H

// TernaryKeywordNode contains token like : {samecycle, over, under, between}
class TernaryKeywordNode : public Node
{
public:
    // Default Constructor
    TernaryKeywordNode() : Node(){};

    // parameterized constructor
    TernaryKeywordNode(std::string value, Node *l, Node *r) : Node(value, l, r){};

public:
    // This function will return pair of string in which first string will contain sql query for an operation and other string would be empty.
    std::pair<std::string, std::string> getSqlQueryString(std::pair<std::string, std::string> returnedLeftPair, std::pair<std::string, std::string> returnedRightpair, std::string temp, TypeofKeyword keyword, TypeofKeyword keywordofParent);
    
};

#endif
