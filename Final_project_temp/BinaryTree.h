#include <string>
#include <vector>

#include "Node.h"

#ifndef BINARYTREE_H
#define BINARYTREE_H



class BinaryTree {
    public: 
    Node* head;

    public:
    //default constructor
    BinaryTree():head(NULL){};

    // constructor for binary tree. it takes argument as a vector.
    BinaryTree(std::vector<std::string>postfixNotationTokens);

    // post order traversal of tree. and it will return sql query.
    std::pair<std::string , std::string> PostOrderTraversal(Node* head , Node* parent);

    // this will deallocate the memory of constructed expression tree.
    void deallocateMemoryoftree(Node* head);

    // destructor for the created object.
    ~BinaryTree();
};

#endif
