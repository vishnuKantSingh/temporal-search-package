#include <string>
#include <vector>
#include <utility>

#include "Global.h"
#include "Node.h"

#ifndef BINARYNODE_H
#define BINARYNODE_H

// BinaryNode contains token like :{!, ==, !=, and, or} all these are operators.
class BinaryNode : public Node{
    public:

    BinaryNode():Node(){};
   BinaryNode(std::string value,Node* l, Node* r) : Node(value , l, r){};     
    
    public:

        // This function will return pair of string in which first string will contain sql query for an operation and other string would be empty.
        std::pair<std::string , std::string> getSqlQueryString(std::pair<std::string , std::string> returnedTeftpair , std::pair<std::string , std::string> returnedRightpair , std::string temp, TypeofKeyword keyword , TypeofKeyword keywordofparent);
    
};
#endif
