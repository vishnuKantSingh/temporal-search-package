#include <string>
#include <map>
#include <vector>
#include <utility>

#include "BinaryNode.h"
#include "Global.h"
#include "Node.h"

// Returns the SQL query (for a node which contains a binary operator) as a first string of returning pair.
std::pair<std::string, std::string> BinaryNode::getSqlQueryString(std::pair<std::string, std::string> returnedLeftPair, std::pair<std::string, std::string> returnedRightPair, std::string temp, TypeofKeyword keywordType, TypeofKeyword keywordTypeofParent)
{
    std::pair<std::string, std::string> returnpair;
    std::string firststring = "";
    std::string secondstring = "";

    if (keywordType == TypeofKeyword::NOT)
    {
        std::cout << " keyword of current node is not and ";
        if ((keywordTypeofParent == TypeofKeyword::AND) || (keywordTypeofParent == TypeofKeyword::OR) || (keywordTypeofParent == TypeofKeyword::FOLLOWEDBY))
        {
            std::cout << " keyword of parent node is and/or/followedby \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE name == '" + returnedRightPair.first + "' AND NOT(value !=0)";
            secondstring += "NOT";
        }
        else if ((keywordTypeofParent == TypeofKeyword::EQUAL) || (keywordTypeofParent == TypeofKeyword::NOTEQUAL))
        {
            std::cout << " keyword of parent node is equal/notequal \n";
            firststring += "SELECT DISTINCT time , NOT(value) FROM '#RequiredTable' WHERE name == '" + returnedRightPair.first + "'";
            secondstring += "NOT";
        }
        else
        {
            std::cerr << "ERROR: Binary node(!) makes the whole expression invalid.";
        }
    }
    else if (keywordType == TypeofKeyword::AND)
    {
        std::cout << " keyword of current node is and \n";
        firststring += " SELECT DISTINCT time FROM '#RequiredTable' WHERE time IN (\n";
        firststring += "\t" + returnedLeftPair.first;
        firststring += "\nINTERSECT \n";
        firststring += "\t" + returnedRightPair.first + "\n" + " )\n";
    }
    else if (keywordType == TypeofKeyword::OR)
    {
        std::cout << " keyword of current node is or \n";
        firststring += " SELECT DISTINCT time FROM '#RequiredTable' WHERE time IN (\n";
        firststring += "\t" + returnedLeftPair.first;
        firststring += "\nUNION \n";
        firststring += "\t" + returnedRightPair.first + "\n" + " )\n";
    }

    else if (keywordType == TypeofKeyword::EQUAL)
    {
        std::cout << " keyword of current node is equal ";
        if (returnedRightPair.second == "SIGNAL")
        {
            std::cout << " this case is signal == signal \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE (time, value) IN (\n";
            firststring += "\t" + returnedLeftPair.first;
            firststring += "\nINTERSECT \n";
            firststring += "\t" + returnedRightPair.first + " )\n";
        }
        else if (returnedRightPair.second == "VALUE")
        {
            std::cout << " this case is signal == value \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE (time, value) IN (\n";
            firststring += "\t" + returnedLeftPair.first;
            firststring += "\t and (value == " + returnedRightPair.first + " ) )\n"; 
        }
        else if (returnedRightPair.second == "NOT")
        {
            std::cout << " this case is signal ==  not(signal) \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE (time, value) IN (\n";
            firststring += "\t" + returnedLeftPair.first;
            firststring += "\nINTERSECT \n";
            firststring += "\t" + returnedRightPair.first + " )\n";
        }
        else
        {
            std::cerr << "ERROR: Binary node(==) makes the whole expression invalid.\n";
        }
    }
    else if (keywordType == TypeofKeyword::NOTEQUAL)
    {
        std::cout << " keyword of current node is notequal ";
        if (returnedRightPair.second == "SIGNAL")
        {
            std::cout << " this case is signal !=  signal \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE (time, value) IN (\n";
            firststring += "\t" + returnedLeftPair.first;
            firststring += "\nEXCEPT \n";
            firststring += "\t" + returnedRightPair.first + " )\n";
        }
        else if (returnedRightPair.second == "VALUE")
        {
            std::cout << " this case is signal !=  value \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE (time, value) IN (\n";
            firststring += "\t" + returnedLeftPair.first;
            firststring += "\t and (value == " + returnedRightPair.first + " )\n";
        }
        else if (returnedRightPair.second == "NOT")
        {
            std::cout << " this case is signal !=  not(signal) \n";
            firststring += "SELECT DISTINCT time FROM '#RequiredTable' WHERE (time, value) IN (\n";
            firststring += "\t" + returnedLeftPair.first;
            firststring += "\nEXCEPT \n";
            firststring += "\t" + returnedRightPair.first + " )\n";
        }
        else
        {
            std::cerr << "ERROR: Binary node(!=) makes the whole expression invalid.";
        }
    }
    else
    {
        std::cerr << "ERROR: BinaryNode:: keywordtype is invalid.";
    }

    returnpair.first = firststring;
    returnpair.second = secondstring;

    return returnpair;
}
