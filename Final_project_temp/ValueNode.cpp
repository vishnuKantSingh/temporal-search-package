#include <string>
#include <map>
#include<vector>
#include<utility>


#include "Global.h"
#include "Node.h"
#include "ValueNode.h"

// Returns the SQL query (for a node which contain value) as a first string of returning pair.
// for a value node SQL query will be value in form of string.
std::pair<std::string , std::string> ValueNode::getSqlQueryString(std::pair<std::string , std::string> returnedLeftPair , std::pair<std::string , std::string> returnedRightPair , std::string temp, TypeofKeyword keywordType , TypeofKeyword keywordtypeofParent){
    std::pair<std::string , std::string> returnpair;
    std::string firststring = ""; 
    std::string secondstring = "";
    
    if(returnedRightPair.second == "VALUE"){
        firststring = temp;
        secondstring = returnedRightPair.first;
    }   
    else {
        firststring = temp;
        secondstring = "VALUE";
    } 
    std::cout<<" keyword of current node is value \n";
    returnpair.first = firststring;
    returnpair.second = secondstring;
     
    return returnpair; 
}
