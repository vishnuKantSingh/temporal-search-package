#include <string>
#include <vector>
#include <utility>

#include "Global.h"
#include "Node.h"

#ifndef SIGNALNODE_H
#define SIGNALNODE_H

// SignalNode contains token only of signal name
class SignalNode : public Node{
    public:
    //Default constructor
    SignalNode():Node(){} ;

    //parameterized constructor
    SignalNode(std::string value,Node* l, Node* r) : Node(value , l, r){};

    public:
        // This function will return pair of string in which first string will contain sql query for a signal name and other string would be empty.
        std::pair<std::string , std::string> getSqlQueryString(std::pair<std::string , std::string> returnedLeftPair , std::pair<std::string , std::string> returnedRightPair , std::string temp, TypeofKeyword keywordType , TypeofKeyword keywordtypeofParent);
    
};


#endif
