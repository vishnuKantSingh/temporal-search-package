#include <string>
#include <map>
#include<vector>
#include<utility>

#include "TernaryKeywordNode.h"
#include "Global.h"
#include "Node.h"

// Returns the SQL query (for a node which contains a ternaryKeyword operator which can be SAMECYCLE, OVER, UNDER, BETWEEN) as a first string of returning pair.
std::pair<std::string , std::string> TernaryKeywordNode::getSqlQueryString(std::pair<std::string , std::string> returnedLeftPair , std::pair<std::string , std::string> returnedRightPair , std::string temp, TypeofKeyword keywordType , TypeofKeyword keywordtypeofParent){
    std::pair<std::string , std::string> returnpair;
    std::string firststring = ""; 
    std::string secondstring ="";

    firststring += " SELECT DISTINCT A.time FROM '#RequiredTable' AS A, '#RequiredTable' AS B WHERE\n";
	    firststring += " A.time IN (\n";
        firststring += returnedLeftPair.first	+" "+" )\n";				
	    firststring += " AND "; // sql keyword of followedby .
	    firststring += " B.time IN (\n";	    	    
        firststring += returnedLeftPair.second + " " + " )\n" ;
	    


    if(keywordType == TypeofKeyword::SAMECYCLE){
        std::cout<<"keyword of current node is samecycle \n";
        firststring += " AND (B.time = A.time) ";

    }
    else if(keywordType == TypeofKeyword::OVER){
        std::cout<<"keyword of current node is over \n";
        firststring += " AND ((B.time - A.time) > "+ returnedRightPair.first +" )" ;
    }
    else if(keywordType == TypeofKeyword::UNDER){
        std::cout<<"keyword of current node is under \n";
        firststring += " AND ((B.time - A.time) < "+ returnedRightPair.first +" )" ;
    }
    else if(keywordType == TypeofKeyword::BETWEEN){
        std::cout<<"keyword of current node is between \n";
        firststring += " AND ((B.time - A.time) BETWEEN "+ returnedRightPair.first +" AND "+ returnedRightPair.second+" ) ";
    }
    else{
        std::cerr<<"ERROR: TernaryKeywordNode:: keywordtype is invalid.\n";
    }

    returnpair.first = firststring;
    returnpair.second = secondstring;

    return returnpair;
}
