#include <string>
#include <map>
#include<vector>
#include<utility>


#include "SignalNode.h"
#include "Global.h"
#include "Node.h"

// Returns the SQL query (for a node which contains a signal name) as a first string of returning pair.
std::pair<std::string , std::string> SignalNode::getSqlQueryString(std::pair<std::string , std::string> returnedLeftPair , std::pair<std::string , std::string> returnedRightPair , std::string temp, TypeofKeyword keywordType , TypeofKeyword keywordtypeofParent){
    std::pair<std::string , std::string> returnpair;
    std::string firststring = ""; 
    std::string secondstring = "";
    std::cout<<" keyword of current node is signal and";
    if((keywordtypeofParent == TypeofKeyword::AND) || (keywordtypeofParent == TypeofKeyword::OR) || (keywordtypeofParent == TypeofKeyword::FOLLOWEDBY)){
        std::cout<<" keyword of parent is and/or/followedby \n";
        firststring += " SELECT DISTINCT time FROM '#RequiredTable' WHERE name == '" + temp +"' AND (value != 0)\n";
        secondstring = "SIGNAL";
    }
    else if((keywordtypeofParent == TypeofKeyword::EQUAL) || (keywordtypeofParent == TypeofKeyword::NOTEQUAL)){
        std::cout<<" keyword of parent is equal/notequal \n";
        firststring += " SELECT DISTINCT time , value FROM '#RequiredTable' WHERE name == '" + temp +"'";
        secondstring = "SIGNAL";
    }
    else if(keywordtypeofParent == TypeofKeyword::NOT){
        std::cout<<" keyword of parent is not \n";
        firststring += temp ;
        secondstring = "SIGNAL";
    }
    else{
        std::cerr << "ERROR: Signal node makes the whole expression invalid."<<"\n";
    }


    returnpair.first = firststring;
    returnpair.second = secondstring;
     
    return returnpair; 
}




