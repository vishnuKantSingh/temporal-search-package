#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <regex>
#include <sstream>
#include <unordered_set>
#include <sqlite3.h>

#include "Global.h"
#include "Tokenizer.h"
#include "Node.h"
#include "BinaryTree.h"
#include "ValueNode.h"
#include "SignalNode.h"
#include "BinaryNode.h"
#include "TernaryNode.h"
#include "TernaryKeywordNode.h"

// This function will return token string from its symbol.
std::string getTokenString(std::string str)
{
    std::map<std::string, std::regex>::const_iterator itr;
    for (itr = TokenSymbol.begin(); itr != TokenSymbol.end(); itr++)
    {
        if (regex_match(str, itr->second))
        {
            return itr->first;
        }
    }
    return "ERROR: in returning the string";
}

//  This gives the keyword of Token symbol.
TypeofKeyword getKeyWord(std::string str)
{
    std::map<TypeofKeyword, std::regex>::const_iterator itr;
    for (itr = KeywordSymbol.begin(); itr != KeywordSymbol.end(); itr++)
    {
        if (regex_match(str, itr->second))
        {
            return itr->first;
        }
    }
    return TypeofKeyword::SIGNAME;
}

// This will convert from enum to string.
std::string enumToString(TypeofKeyword value)
{
    if (value == TypeofKeyword::NOT)
    {
        return "NOT";
    }
    else if (value == TypeofKeyword::NOTEQUAL)
    {
        return "NOTEQUAL";
    }
    else if (value == TypeofKeyword::EQUAL)
    {
        return "EQUAL";
    }
    else if (value == TypeofKeyword::AND)
    {
        return "AND";
    }
    else if (value == TypeofKeyword::OR)
    {
        return "OR";
    }
    if (value == TypeofKeyword::FOLLOWEDBY)
    {
        return "FOLLOWEDBY";
    }
    else if (value == TypeofKeyword::SAMECYCLE)
    {
        return "SAMECYCLE";
    }
    else if (value == TypeofKeyword::OVER)
    {
        return "OVER";
    }
    else if (value == TypeofKeyword::UNDER)
    {
        return "UNDER";
    }
    else if (value == TypeofKeyword::BETWEEN)
    {
        return "BETWEEN";
    }
    else
    {
        std::cerr << "ERROR: enum to string conversion is not possible.";
        return "ERROR: ";
    }
}

// This function will return the precedence of an operator
int getPrecedence(std::string op)
{
    std::map<TypeofKeyword, int>::const_iterator itr;
    for (itr = precedence.begin(); itr != precedence.end(); itr++)
    {
        if (op == enumToString(itr->first))
        {
            return itr->second;
        }
    }
    std::cout << "\n"
              << "operator is invalid ."
              << "\n";
    return -1;
}

// Tells about string token is operator or not
bool isOp(std::string str)
{
    std::map<int, std::regex>::const_iterator itr;
    for (itr = opSet.begin(); itr != opSet.end(); itr++)
    {
        if (regex_match(str, itr->second))
        {
            return true;
        }
    }
    return false;
}

// This will convert all type of values into decimal(for exp: value types can be binary,hexadecimal, octadecimal etc.)
std::string convertToDecimal(std::string token)
{
    token = std::regex_replace(token, std::regex("-?(\\d)?'"), "");

    // convert binary to decimal. 4'b0101 => 5
    if (std::regex_match(token, std::regex("^b[1|0|x|X|Z|z|_]+")))
    {
        token = std::regex_replace(token, std::regex("^b"), "");
        token = std::to_string(stoi(token, 0, 2));
    }

    // convert decimal to readable decimal. 3'd890 => 890
    else if (std::regex_match(token, std::regex("^d[\\d]+")))
    {
        token = std::regex_replace(token, std::regex("^d"), "");
    }

    // convert hex to decimal.
    else if (std::regex_match(token, std::regex("^h[\\d|x|X|_|z|Z|a|A|b|B|c|C|d|D|e|E|f|F]+")))
    {
        std::cout << "\n HEX Operand \n";
        token = std::regex_replace(token, std::regex("^h"), "");
        token = std::to_string(stoi(token, 0, 16));
    }

    // convert octal to decimal
    else if (std::regex_match(token, std::regex("^o[0-7|x|X|z|Z|_]+")))
    {
        token = std::regex_replace(token, std::regex("^o"), "");
        token = std::to_string(stoi(token, 0, 8));
    }
    // decimal value will remain as decimal.
    else if (std::regex_match(token, std::regex("[0-9]+")))
    {
        token = token;
    }
    else
    {
        std::cerr << "ERROR: converting To decimal can not be possible. ";
        exit(0);
    }

    return token;
}

// This function will gives us name of all the signals.
std::unordered_set<std::string> getAllSignals(std::vector<std::string> infixTokens)
{
    std::unordered_set<std::string> allSignals;
    for (int i = 0; i < infixTokens.size(); i++)
    {
        if (getKeyWord(infixTokens[i]) == TypeofKeyword::SIGNAME)
        {
            allSignals.insert(infixTokens[i]);
        }
    }
    return allSignals;
}

// This function will convert Tokens from Infix Notation to Postfix Notation.
std::vector<std::string> getPostTokens(std::vector<std::string> inTokens)
{
    std::stack<std::string> op_stack;
    std::stack<std::string> oprnd_stack;
    std::stack<std::string> temp_stack;
    std::vector<std::string> postToken;

    for (int i = 0; i < inTokens.size(); i++)
    {
        if (inTokens[i] == "(")
        {
            op_stack.push(inTokens[i]);
            std::cout << inTokens[i] << " : pushed into op_stack."
                      << "\n";
        }
        else if (inTokens[i] == ")")
        {
            while (op_stack.top() != "(")
            {
                std::string curr = op_stack.top();
                op_stack.pop();
                std::cout << curr << " : popped from op_stack."
                          << "\n";
                oprnd_stack.push(curr);
                std::cout << curr << " : pushed into oprnd_stack."
                          << "\n";
            }
            op_stack.pop();
        }
        else if (isOp(inTokens[i]))
        {
            while ((!op_stack.empty()) && (op_stack.top() != "(") && (getPrecedence(getTokenString(op_stack.top())) >= getPrecedence(getTokenString(inTokens[i]))))
            {
                std::string curr = op_stack.top();
                op_stack.pop();
                std::cout << curr << " : popped from op_stack."
                          << "\n";
                oprnd_stack.push(curr);
                std::cout << curr << " : pushed into oprnd_stack."
                          << "\n";
            }
            op_stack.push(inTokens[i]);
            std::cout << inTokens[i] << " : pushed into op_stack."
                      << "\n";
        }
        else
        {
            oprnd_stack.push(inTokens[i]);
            std::cout << inTokens[i] << " : pushed into oprnd_stack."
                      << "\n";
        }
    }

    while (!op_stack.empty())
    {
        std::string curr = op_stack.top();
        op_stack.pop();
        oprnd_stack.push(curr);
    }

    while (!oprnd_stack.empty())
    {
        std::string curr = oprnd_stack.top();
        oprnd_stack.pop();
        if (getKeyWord(curr) == TypeofKeyword::VALUE)
        {
            curr = convertToDecimal(curr);
        }
        temp_stack.push(curr);
    }

    while (!temp_stack.empty())
    {
        std::string curr = temp_stack.top();
        temp_stack.pop();
        postToken.push_back(curr);
    }
    return postToken;
}

// compute the height of tree.
int maxDepth(Node *node)
{
    if (node == NULL)
        return 0;
    else
    {
        /* compute the depth of each subtree */
        int lDepth = maxDepth(node->left);
        int rDepth = maxDepth(node->right);

        /* use the larger one */
        if (lDepth > rDepth)
            return (lDepth + 1);
        else
            return (rDepth + 1);
    }
}

// Function to print all nodes of a given level from left to right
bool printLevel(Node *root, int level)
{
    if (root == nullptr)
    {
        return false;
    }

    if (level == 1)
    {
        std::cout << root->value << " ";

        // return true if at least one node is present at a given level
        return true;
    }

    bool left = printLevel(root->left, level - 1);
    bool right = printLevel(root->right, level - 1);

    return left || right;
}

// Function to print level order traversal of a given binary tree
void levelOrderTraversal(Node *root)
{
    // start from level 1 — till the height of the tree
    int level = 1;

    // run till printLevel() returns false
    while (printLevel(root, level))
    {
        level++;
    }
}

// the database like the vcd file, only keeps track of signal changes.  we first need to identify all time steps at which all signals selected, change values. Temporary table #AllTimeSteps generated in by Sql1 will identify all such time steps.
std::string constructSqlTable1(std::vector<std::string> allSignals)
{
    std::string query;

    query = "\tDROP TABLE IF EXISTS '#AllTimeSteps';\n";
    query += "\tCREATE TABLE '#AllTimeSteps' ( `time` NUMERIC NOT NULL);\n";
    query += "\tINSERT INTO '#AllTimeSteps'\n";
    query += "\t\tSELECT DISTINCT time FROM signal_values WHERE\n";
    for (int i = 0; i < allSignals.size(); ++i)
    {
        query += "\t\t\t( code_id  IN ( SELECT code_id FROM signals WHERE name == '" + allSignals[i] + "' ) )";
        if (i != allSignals.size() - 1)
            query += " or ";
        query += "\n";
    }
    query += "\tOrder by time Asc;\n";
    return query;
}

//	This query greps all the vcd entries (time & values) for all the signals selected into table #TimeValuesInVcd .
std::string constructSqlTable2(std::vector<std::string> allSignals)
{
    std::string query;

    query = "\tDROP TABLE IF EXISTS '#TimeValuesInVcd';\n";
    query += "\tCREATE TABLE '#TimeValuesInVcd' ( 'id' Numeric NOT Null,'name' TEXT, 'time' NUMERIC NOT NULL, 'value' TEXT NOT NULL);\n";
    query += "\tINSERT INTO '#TimeValuesInVcd'\n";
    query += "\t\tSELECT signal_values.code_id, signals.name, signal_values.time, signal_values.value \n";
    query += "\t\tFROM signal_values JOIN signals ON signal_values.code_id = signals.code_id WHERE \n";
    for (int i = 0; i < allSignals.size(); ++i)
    {
        query += "\t\t\t(signals.name == '" + allSignals[i] + "' )";
        if (i != allSignals.size() - 1)
            query += " or \n";
        else
            query += ";\n";
    }
    return query;
}

//  This sql statement combines the 2 tables above to obtain time value pair for all signals at all times needed in table #RequiredTable .
std::string constructSqlTable3()
{
    std::string query;

    query = "\tDROP TABLE IF EXISTS '#RequiredTable';\n";
    query += "\tCREATE TABLE '#RequiredTable' ( 'id' Numeric NOT Null, 'name' TEXT, 'time' NUMERIC NOT NULL, 'value' TEXT NOT NULL, 'max_time' NUMERIC NOT NULL);\n";
    query += "\tINSERT INTO '#RequiredTable'\n";
    query += "\t\tSELECT T.id, T.name, A.time, T.value, max(T.time)\n";
    query += "\t\t\tFrom '#AllTimeSteps' AS A, '#TimeValuesInVcd' AS T WHERE T.time <= A.time GROUP BY T.id, A.time;\n";
    return query;
}

static int callback1(void *data, int argc, char **argv, char **azColName)
{
    int i;
    fprintf(stderr, "%s: ", (const char *)data);
    for (i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

// this will convert input expression to its SQL query.
std::string convertInputExpToSqlQuery(std::string input_exp, std::vector<std::string> &allsignals)
{
    // This will intialize the object of tokenizer class  and takes input as a expression(string) and it will return the vector of token in infix notation.
    Tokenizer alltokens1(input_exp);

    // This Function will convert infix tokens to postfix tokens.
    std::cout << "The step by step conversion of infix tokens to postfix tokens : "
              << "\n";
    std::vector<std::string> postfixNotationTokens = getPostTokens(alltokens1.allTokens);
    std::cout << "\n";
    // This will print all postfix tokens.
    for (int i = 0; i < postfixNotationTokens.size(); i++)
    {
        std::cout << "Postfix Token " << i + 1 << " : " << postfixNotationTokens[i] << "\n";
    }
    std::cout << "\n";

    // This will gives name of all the signals.
    // std::vector<std::string> allsignals;
    std::unordered_set<std::string> allSignalset = getAllSignals(postfixNotationTokens);
    std::unordered_set<std::string>::const_iterator itr;
    std::cout << "\n"
              << "List of all the signal names : "
              << "\n";
    for (itr = allSignalset.begin(); itr != allSignalset.end(); itr++)
    {
        allsignals.push_back(*itr);
        std::cout << *itr << "\n";
    }
    std::cout << "\n";

    std::cout << "\n"
              << "The step by step construction of expression tree : "
              << "\n";
    BinaryTree ExpressionTree(postfixNotationTokens);

    // This gives Level Order Traversal of expression tree.
    std::cout << "\n";
    std::cout << "The output of level order traversal of expression tree : "
              << "\n";
    levelOrderTraversal(ExpressionTree.head);
    std::cout << "\n";

    // This gives height of expression tree.
    int ans = maxDepth(ExpressionTree.head);
    std::cout << "\n"
              << "height of Binary Tree : " << ans << "\n";

    // postorder traversal of a constructed binary(expression) tree.
    std::cout << "\n"
              << "The step by step postorder traversal of expression tree : "
              << "\n";
    Node *temp = NULL;
    std::pair<std::string, std::string> returnedpair = ExpressionTree.PostOrderTraversal(ExpressionTree.head, temp);

    std::string queryString = returnedpair.first; // this will be the final sql query of input expression.

    // Connection to the Database
    sqlite3 *db;
    int connectionStatus = sqlite3_open("/home/vishnu/Desktop/tempo3/new.sim.vcd.sqlite", &db);
    char *messageError;
    std::cout << "\n";
    if (connectionStatus)
    {
        std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return (0);
    }
    else
    {
        std::cout << "Opened database successfully\n";
    }
    std::cout << "\n";

    // This query will construct a table '#AllTimeSteps' in which all time steps for all signal selected will be there.
    std::string sqlStatment1 = constructSqlTable1(allsignals);
    std::cout << "\n\nSql Statement #1 Generated:\n"
              << sqlStatment1 << std::endl;

    // connectionStatus = sqlite3_exec(db, sqlStatment1.c_str(), callback1, 0, &messageError);
    if (connectionStatus != SQLITE_OK)
    {
        std::cerr << "Error : " << messageError << std::endl;
        sqlite3_free(messageError);
    }
    else
        std::cout << "Table #AllTimeSteps has been generated." << std::endl;

    //   this query will construct the table '#TimeValuesInVcd' in which all the vcd entries (time & values) for all the signals selected.
    std::string sqlStatment2 = constructSqlTable2(allsignals);
    std::cout << "\n\nSql Statement #2 Generated:\n"
              << sqlStatment2 << std::endl;

    // connectionStatus = sqlite3_exec(db, sqlStatment2.c_str(), callback1, 0, &messageError);
    if (connectionStatus != SQLITE_OK)
    {
        std::cerr << "Error : " << messageError << std::endl;
        sqlite3_free(messageError);
    }
    else
    {
        std::cout << "Table #ValuesAsInVcd has been generated" << std::endl;
    }

    //   This sql will construct a table '#RequiredTable' combines the 2 tables above to obtain time value pair for all signals at all times needed.
    std::string sqlStatment3 = constructSqlTable3();
    std::cout << "\n\nSql Statement #3 Generated:\n"
              << sqlStatment3 << std::endl;

    // connectionStatus = sqlite3_exec(db, sqlStatment3.c_str(), callback1, 0, &messageError);
    if (connectionStatus != SQLITE_OK)
    {
        std::cerr << "Error : " << messageError << std::endl;
        sqlite3_free(messageError);
    }
    else
    {
        std::cout << "Table #RequiredTable has been generated" << std::endl;
    }

    std::cout << "\nQuery Statement Generated:\n"
              << queryString << std::endl; // FINAL QUERY STATEMENT IS HERE.
    return queryString;
}

// For given SQL query , it will return all the required time instances.
std::vector<int> getAllTimeInstancesAsFinalResult(std::string sqlQuery)
{
    std::vector<int> TimeInstances;

    // Connection to the Database
    sqlite3 *db;
    char *messageError;
    std::cout << "\n";

    int connectionStatus = sqlite3_open("/home/vishnu/Desktop/tempo3/new.sim.vcd.sqlite", &db);
    if (connectionStatus)
    {
        std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return TimeInstances;
    }

    sqlite3_stmt *stmt;
    // compile sql statement to binary
    if (sqlite3_prepare_v2(db, sqlQuery.c_str(), -1, &stmt, NULL) != SQLITE_OK)
    {
        printf("ERROR: while compiling sql: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        sqlite3_finalize(stmt);
        return TimeInstances;
    }
    std::cout << "\n SQL Statement Compiled sucessfully. Hurrayyy !!! \n";

    // execute sql statement, and while there are rows returned
    int ret_code = 0;
    while ((ret_code = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        TimeInstances.push_back(sqlite3_column_int(stmt, 0));
    }
    if (ret_code != SQLITE_DONE)
    {
        // error handling
        std::cout << "";
        printf("ERROR: while performing sql: %s\n", sqlite3_errmsg(db));
        printf("ret_code = %d\n", ret_code);
    }

    std::cout << "\n";
    // Printing all the time instances where input expression gives its value true.
    std::cout << "Time Instances(where input expression gives its value true) are given below : \n";
    for (int i = 0; i < TimeInstances.size(); i++)
    {
        std::cout << "Time"
                  << " = " << TimeInstances[i] << "\n";
    }

    //  closing the database.
    sqlite3_close(db);
    std::cout << "\nDatabase has been closed.\n";

    // destructor to relese the memory.
    BinaryTree bt;
    bt.~BinaryTree();

    return TimeInstances;
}

int main()
{
    // input expression
    std::string input_exp = "m==0 Or q";
 
    //exp 1
        // std::string input_exp = "(swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk == swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode)  AnD (swerv_wrapper.swerv.dec.tlu.mice_ce_req == swerv_wrapper.swerv.dec.tlu.minstret_enable Or swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f == swerv_wrapper.swerv.dec.tlu.mpc_run_state_f)";
    //exp 2
        // std::string input_exp = "(swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode==1'b0)  AnD (swerv_wrapper.swerv.dec.tlu.mice_ce_req!=swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f Or swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f== !swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy) FollowedBy swerv_wrapper.swerv.ifu.ic_hit_f2 SameCycle";
    //exp 3
        // std::string input_exp = "(swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == !swerv_wrapper.swerv.dec.tlu.mice_ce_req)  AnD (swerv_wrapper.swerv.dec.tlu.mice_ce_req==swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy Or swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy==swerv_wrapper.swerv.ifu.ic_hit_f2) FollowedBy swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f between 2 5";
    //exp 4
        // std::string input_exp = "(swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode AnD (!swerv_wrapper.swerv.dec.tlu.mice_ce_req ==swerv_wrapper.swerv.ifu.ic_hit_f2 Or !swerv_wrapper.swerv.dec.tlu.mice_ce_req==swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk) FollowedBy swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f undeR 2)";
    //exp 5
        // std::string input_exp = "(swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == !swerv_wrapper.swerv.dec.tlu.mice_ce_req)  AnD (swerv_wrapper.swerv.dec.tlu.mice_ce_req==swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy Or swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy==swerv_wrapper.swerv.ifu.ic_hit_f2) FollowedBy swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == !swerv_wrapper.swerv.dec.tlu.mice_ce_req and swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == !swerv_wrapper.swerv.dec.tlu.mice_ce_req between 2 5";
    //exp 6
        // std::string input_exp = "((swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode==swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk)  AnD (swerv_wrapper.swerv.dec.tlu.mice_ce_req==swerv_wrapper.swerv.ifu.ic_hit_f2 Or swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f==swerv_wrapper.swerv.dec.tlu.mice_ce_req) FollowedBy swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk and swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f!=swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy between 2 5) and swerv_wrapper.swerv.ifu.ic_hit_f2 followedBy !swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk Over 10";
    //exp 7
        // std::string input_exp = "((swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk)  AnD (swerv_wrapper.swerv.dec.tlu.mice_ce_req==swerv_wrapper.swerv.ifu.ic_hit_f2 Or swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f==swerv_wrapper.swerv.dec.tlu.mice_ce_req) FollowedBy swerv_wrapper.swerv.dec.tlu.miccmect_ff.scan_mode == swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk and swerv_wrapper.swerv.dec.tlu.mpc_halt_state_f!=swerv_wrapper.swerv.ifu.ifc.ic_crit_wd_rdy between 2 5  followedBy !swerv_wrapper.swerv.dec.tlu.miccmect_ff.clk Over 10)";   
  
    std::vector<std::string> allsignals; // This contains all the signals which is present in the input expression.



    // this will convert input expression to its SQL query.
    std::string sqlQuery = convertInputExpToSqlQuery(input_exp, allsignals);

    // For given SQL query , it will return all the required time instances.
    std::vector<int> TimeInstances = getAllTimeInstancesAsFinalResult(sqlQuery);

    return 0;
}